package ru.ohotnik.rest.megafon;

import com.google.common.base.Splitter;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.Callable;

import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;

/**
 * Created by ohotNik
 * Date : 13.10.14
 * Time : 1:23
 * Description : Содержит данные о файле по топику и времени.
 */
class FileEntry implements Callable<FileEntry> {
  private long size;
  private Map<Integer, String> content = newHashMap();
  private LongSummaryStatistics statistics;
  private final String filepath;

  protected long getSize() {
    return size;
  }

  protected FileEntry(String filepath) {
    this.filepath = filepath;
  }

  protected LongSummaryStatistics getStat() {
    return statistics;
  }

  public Map<Integer, String> getContent() {
    return Collections.unmodifiableMap(content);
  }

  /**
   * Computes a result, or throws an exception if unable to do so.
   *
   * @return computed result
   * @throws Exception if unable to compute a result
   */
  @SuppressWarnings("ProhibitedExceptionDeclared")
  @Override
  public FileEntry call() throws Exception {
    File file = new File(filepath);
    List<String> lines = newArrayList();
    Scanner in;
    try {
      in = new Scanner(file);
    } catch (FileNotFoundException ignored) {
      size = 0L;
      return this;
    }
    while (in.hasNextLine()) {
      lines.add(in.nextLine());
    }

    size = file.length();

    for (String line : lines) {
      if (!isNullOrEmpty(line)) {
        ArrayList<String> values = newArrayList(Splitter.on(",").split(line));
        content.put(Integer.parseInt(values.get(0)), values.get(1));
      }
    }
    statistics = content.values().stream().mapToLong(Long::valueOf).summaryStatistics();
    return this;
  }
}

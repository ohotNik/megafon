package ru.ohotnik.rest.megafon;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import static com.google.common.collect.Sets.newIdentityHashSet;
import static java.util.Collections.sort;
import static ru.ohotnik.rest.megafon.FileCache.getAverage;
import static ru.ohotnik.rest.megafon.FileCache.getContent;
import static ru.ohotnik.rest.megafon.FileCache.getMax;
import static ru.ohotnik.rest.megafon.FileCache.getMin;
import static ru.ohotnik.rest.megafon.FileCache.getPartitions;
import static ru.ohotnik.rest.megafon.FileCache.getSumm;

/**
 * Created by ohotNik
 * Date : 11.10.14
 * Time : 19:31
 * Description :Класс принимает запросы и отрисовывает результаты.
 */

@SuppressWarnings({
    "HardCodedStringLiteral", "HardcodedFileSeparator"
    , "MethodMayBeStatic"})
@Controller
@EnableAutoConfiguration
public class MainClass {

  @SuppressWarnings({"StaticVariableMayNotBeInitialized", "StaticVariableNamingConvention"})
  private static TopicsScanner scanner;

  /**
   * Отображение всех топиков.
   *
   * @return -
   */
  @RequestMapping("/topics")
  @ResponseBody
  String topicsRequest() {
    return Joiner.on("<br/>").join(scanner.getTopics());
  }

  /**
   * Отображение последних вызовов по топикам.
   *
   * @return -
   */
  @RequestMapping("/topics/last")
  @ResponseBody
  String topicsLastRequest() {

    StringBuilder builder = new StringBuilder();
    builder.append("<table border='1'><tr><th colspan='2'>Last timestamps</th></tr>");

    Map<String, String> timestamps = scanner.getLastTimestamps();
    String row = "<tr><td>%s</td><td>%s</td></tr>";
    //noinspection KeySetIterationMayUseEntrySet
    for (String topic : timestamps.keySet()) {
      builder.append(String.format(row, topic, timestamps.get(topic)));
    }

    builder.append("</table>");

    return builder.toString();
  }

  /**
   * Отображение статистики по последним запросам.
   *
   * @return -
   */
  @RequestMapping("/topics/stat")
  @ResponseBody
  String topicsStat() {

    StringBuilder builder = new StringBuilder();
    builder.append("<table border='1'><tr><th colspan='4'>Statistic</th></tr>");
    builder.append("<tr><td>Topic</td><td>Summ</td><td>Max/Min</td><td>Average</td></tr>");

    Map<String, String> lastTimestamps = scanner.getLastTimestamps();
    Set<String> topics = lastTimestamps.keySet();
    for (String topic : topics) {
      String path = scanner.getOffsetPath(topic, lastTimestamps.get(topic));

      builder.append("<tr>");
      builder.append("<td>").append(topic).append("</td>");
      builder.append("<td>").append(getSumm(path)).append("</td>");
      builder.append("<td>").append(getMax(path)).append('/').append(getMin(path)).append("</td>");
      builder.append("<td>").append(getAverage(path)).append("</td>");
      builder.append("</tr>");
    }

    builder.append("</table>");
    return builder.toString();
  }

  /**
   * Отображение статистики для топиков.
   *
   * @return -
   * @throws ExecutionException
   */
  @RequestMapping("/topics/total")
  @ResponseBody
  String topicsTotal() throws ExecutionException {
    StringBuilder builder = new StringBuilder();
    builder.append("<table border='1'><tr><th colspan = '3'>Total messages in last timestamp</th></tr>");
    builder.append("<tr><td>topic</td><td>All partitions</td><td>Last messages</td>");
    List<String> topics = scanner.getTopics();
    Map<String, String> lastTimestamps = scanner.getLastTimestamps();
    for (String topic : topics) {
      List<Date> times = scanner.getAllTimestamps(topic);
      Set<Integer> partitions = newIdentityHashSet();
      for (Date timestamp : times) {
        partitions.addAll(getPartitions(scanner.getOffsetPath(topic, timestamp)));
      }

      List<Integer> parList = Lists.newArrayList(partitions);
      sort(parList);
      if (parList.isEmpty()) {
        builder.append("<tr><td>").append(topic).append("</td><td colspan='2'>Not found</td></tr>");
      } else {
        builder.append(getRow(topic, parList, lastTimestamps.get(topic)));
      }
    }
    builder.append("</table>");
    return builder.toString();
  }

  /**
   * Служебный. Отрисовка отдельной строки для статистики топика.
   *
   * @param topic      имя топика
   * @param partitions все доступные партиции в топике
   * @param time       последний вызов в топике
   * @return -
   * @throws ExecutionException
   */
  private static String getRow(String topic, Collection<Integer> partitions, String time) throws ExecutionException {
    //noinspection StaticVariableUsedBeforeInitialization
    Map<Integer, String> content = getContent(scanner.getOffsetPath(topic, time));
    boolean first = true;
    StringBuilder row = new StringBuilder();
    for (Integer part : partitions) {
      String msg = content.get(part);
      if (msg == null) {
        msg = "";
      }
      if (first) {
        first = false;
        row.append("<tr><td rowspan='").append(partitions.size()).append("'>").append(topic).append("</td>");
        row.append("<td>").append(part).append("</td><td>").append(msg).append("</td></tr>");
      } else {
        row.append("<tr><td>").append(part).append("</td><td>").append(msg).append("</td></tr>");
      }
    }
    return row.toString();
  }


  public static void main(String... args) throws IOException {
    if (args.length == 0) {
      throw new IllegalArgumentException("Need directory argument!");
    }

    String path = args[0];
    File destDir = new File(path);
    if (!destDir.exists() || !destDir.isDirectory()) {
      throw new IOException("Illegal directory. May be it was not created?");
    }

    scanner = new TopicsScanner(path);

    SpringApplication.run(MainClass.class, args);
  }

}

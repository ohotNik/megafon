package ru.ohotnik.rest.megafon;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import java.io.File;
import java.util.LongSummaryStatistics;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * Created by ohotNik
 * Date : 12.10.14
 * Time : 20:29
 * Description :  Обеспечивает работу кэшера и предоставляет базовую статистику.
 */
public class FileCache {

  /**
   * используется для минимизации обращений в диску. хранит данные по прочитанным файлам.
   * ключ - полный путь до файла.
   * значение - {@link ru.ohotnik.rest.megafon.FileEntry} инвалидируется при изменении размера файла.
   */
  private static final Cache<String, FileEntry> FILE_CACHE = CacheBuilder.newBuilder().expireAfterWrite(1L,
      TimeUnit.HOURS).build();

  private FileCache() {
  }

  public static Long getSumm(String path) {
    try {
      LongSummaryStatistics stat = getFileEntry(path).getStat();
      return stat != null ? stat.getSum() : 0L;
    } catch (ExecutionException ex) {
      //noinspection CallToPrintStackTrace
      ex.printStackTrace();
    }
    return 0L;
  }

  public static Long getMax(String path) {
    try {
      LongSummaryStatistics stat = getFileEntry(path).getStat();
      return stat != null ? stat.getMax() : 0L;
    } catch (ExecutionException ex) {
      //noinspection CallToPrintStackTrace
      ex.printStackTrace();
    }
    return 0L;
  }

  public static Long getMin(String path) {
    try {
      LongSummaryStatistics stat = getFileEntry(path).getStat();
      return stat != null ? stat.getMin() : 0L;
    } catch (ExecutionException ex) {
      //noinspection CallToPrintStackTrace
      ex.printStackTrace();
    }
    return 0L;
  }

  public static double getAverage(String path) {
    try {
      LongSummaryStatistics stat = getFileEntry(path).getStat();
      return stat != null ? stat.getAverage() : 0.0;
    } catch (ExecutionException ex) {
      //noinspection CallToPrintStackTrace
      ex.printStackTrace();
    }
    return 0.0;
  }

  public static Set<Integer> getPartitions(String path) throws ExecutionException {
    return getContent(path).keySet();
  }

  public static Map<Integer, String> getContent(String path) throws ExecutionException {
    FileEntry entry = getFileEntry(path);
    return entry.getContent();
  }

  /**
   * получить значение {@link ru.ohotnik.rest.megafon.FileEntry} из кэша или же создать.
   * выполняется проверка на необходимость инвалидации - по размеру файла.
   *
   * @param path путь до файла с данными
   * @return -
   * @throws ExecutionException
   */
  private static FileEntry getFileEntry(String path) throws ExecutionException {

    FileEntry entry = FILE_CACHE.getIfPresent(path);
    if (entry == null) {
      return FILE_CACHE.get(path, new FileEntry(path));
    }

    // примем, что манипуляций с содержимым файлов нет. иначе следует усложнить проверку.
    File file = new File(path);
    if (file.length() != entry.getSize()) {
      FILE_CACHE.invalidate(path);
      entry = FILE_CACHE.get(path, new FileEntry(path));
    }

    return entry;
  }

}

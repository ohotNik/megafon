package ru.ohotnik.rest.megafon;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static java.lang.String.format;

/**
 * Created by ohotNik
 * Date : 12.10.14
 * Time : 16:52
 * Description :
 */
public class TopicsScanner {

  private static final String TIMESTAMP_PATH = "%s/history/";
  private static final String DEFAULT_FILE = "offsets.csv";
  private static final DateFormat DATE_FORMAT =
      new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.getDefault());

  private final String scanDir;


  public TopicsScanner(String path) {
    scanDir = path;
  }

  public String getOffsetPath(String topic, Date date) {
    //noinspection AccessToNonThreadSafeStaticField
    return getOffsetPath(topic, DATE_FORMAT.format(date));
  }

  /**
   * Просчет ключа для кэшера.
   *
   * @param topic название топика
   * @param time  время
   * @return -
   */
  public final String getOffsetPath(String topic, String time) {
    return scanDir + '/' + format(TIMESTAMP_PATH, topic) + time + '/' + DEFAULT_FILE;
  }

  public List<String> getTopics() {
    List<String> topics = newArrayList();
    try {
      Files.walk(Paths.get(scanDir), 1).forEach(filePath -> {
        topics.add(filePath.getFileName().toString());
      });
      topics.remove(0);
      return topics;
    } catch (IOException ex) {
      //noinspection CallToPrintStackTrace
      ex.printStackTrace();
    }
    return topics;
  }

  public Map<String, String> getLastTimestamps() {
    Map<String, String> result = newHashMap();
    List<String> topics = getTopics();
    for (String topic : topics) {
      String lastTs = getLastTimestampByTopic(topic);
      if (isNullOrEmpty(lastTs)) {
        result.put(topic, "Not found");
      } else {
        result.put(topic, lastTs);
      }
    }

    return result;
  }

  public String getLastTimestampByTopic(String topic) {
    List<Date> timestamps = getAllTimestamps(topic);
    if (!timestamps.isEmpty()) {
      Date max = Collections.max(timestamps);
      //noinspection AccessToNonThreadSafeStaticField
      return DATE_FORMAT.format(max);
    }
    return "";
  }

  public List<Date> getAllTimestamps(String topic) {
    String path = format(TIMESTAMP_PATH, scanDir + '/' + topic);
    List<Date> timestamps = newArrayList();
    try {
      Files.walk(Paths.get(path), 1).forEach(filePath -> {
        try {
          //noinspection AccessToNonThreadSafeStaticField
          timestamps.add(DATE_FORMAT.parse(filePath.getFileName().toString()));
        } catch (ParseException ignored) {
        }
      });
    } catch (IOException ex) {
      //noinspection CallToPrintStackTrace
      ex.printStackTrace();
    }
    return timestamps;
  }

}

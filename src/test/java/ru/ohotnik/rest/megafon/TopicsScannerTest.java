package ru.ohotnik.rest.megafon;

import com.google.common.base.Strings;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.List;
import java.util.Map;

import static java.lang.String.valueOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by ohotNik
 * Date : 15.10.14
 * Time : 16:50
 * Description : test for {@link TopicsScanner}
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class TopicsScannerTest {

  private String folder;

  @Before
  public void before() {
    folder = valueOf(getClass().getResource("/data")).replace("file:/", "");
    assertFalse("Test directory is missing", Strings.isNullOrEmpty(folder));
  }


  /**
   * {@link TopicsScanner#getTopics()}
   *
   * @throws Exception
   */
  @Test
  public void testGetTopics() throws Exception {
    System.out.println("test method getTopics");

    TopicsScanner testObject = new TopicsScanner(folder);
    List<String> actual = testObject.getTopics();
    assertFalse("Nothing was found!", actual.isEmpty());
    assertEquals("Wrong size!", 3L, (long) actual.size());
  }

  /**
   * {@link TopicsScanner#getLastTimestamps()}
   */
  @Test
  public void testGetLastTimestamps() {
    System.out.println("test method getLastTimestamps");

    TopicsScanner testObj = new TopicsScanner(folder);
    Map<String, String> lastTimestamps = testObj.getLastTimestamps();
    assertEquals("Wrong result!", "2014-10-12-18-37-07", lastTimestamps.get("topic1"));
  }

  /**
   * {@link TopicsScanner#getLastTimestampByTopic(String)}
   */
  @Test
  public void testGetLastTimestampByTopic() {
    System.out.println("test method getLastTimestampByTopic");

    TopicsScanner testObj = new TopicsScanner(folder);
    String lastTimestampByTopic = testObj.getLastTimestampByTopic("topic1");
    assertEquals("Wrong result!", "2014-10-12-18-37-07", lastTimestampByTopic);
  }

  /**
   * {@link TopicsScanner#getOffsetPath(String, java.util.Date)}
   */
  @Test
  public void testGetOffsetPath() {
    System.out.println("test method getOffsetPath");

    Date date = new Date();
    date.setTime(1413380144715L);
    TopicsScanner testObj = new TopicsScanner(folder);
    String path = testObj.getOffsetPath("topic1", date);
    assertTrue("Wrong path evaluation", path.endsWith("/topic1/history/2014-10-15-17-35-44/offsets.csv"));
  }
}

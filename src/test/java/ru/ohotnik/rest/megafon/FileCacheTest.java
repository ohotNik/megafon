package ru.ohotnik.rest.megafon;

import com.google.common.base.Strings;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static java.lang.String.valueOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by ohotNik
 * Date : 15.10.14
 * Time : 18:42
 * Description :
 */
public class FileCacheTest {


  private String path;

  @Before
  public void setUp() throws Exception {
    String folder = valueOf(getClass().getResource("/data")).replace("file:/", "");
    assertFalse("Test directory is missing", Strings.isNullOrEmpty(folder));
    TopicsScanner scanner = new TopicsScanner(folder);
    path = scanner.getOffsetPath("topic1", "2014-10-12-18-37-05");
  }

  @Test
  public void testCommon() throws Exception {
    //noinspection UseOfSystemOutOrSystemErr
    System.out.println("test methods");
    assertEquals("getMax error", 180L, (long) FileCache.getMax(path));
    assertEquals("getMin error", 10L, (long) FileCache.getMin(path));
    assertEquals("getSumm error", 1717L, (long) FileCache.getSumm(path));
    //noinspection NumericCastThatLosesPrecision
    assertTrue("getAverage error", Math.abs(95.0 - FileCache.getAverage(path)) < 1.0);

    Map<Integer, String> content = FileCache.getContent(path);
    assertFalse("Wrong content", content.keySet().isEmpty());
    assertEquals("Wrong partitions set", 18L, (long) FileCache.getPartitions(path).size());

  }
}

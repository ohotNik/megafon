package ru.ohotnik.rest.megafon;

import com.google.common.base.Strings;
import org.junit.Before;
import org.junit.Test;

import static java.lang.String.valueOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by ohotNik
 * Date : 15.10.14
 * Time : 17:42
 * Description :
 */
@SuppressWarnings("UseOfSystemOutOrSystemErr")
public class FileEntryTest {

  private String folder;

  @Before
  public void setUp() throws Exception {
    folder = valueOf(getClass().getResource("/data")).replace("file:/", "");
    assertFalse("Test directory is missing", Strings.isNullOrEmpty(folder));
  }

  /**
   * {@link FileEntry#getSize()}, {@link FileEntry#call()}, {@link FileEntry#getContent()}, {@link FileEntry#getStat()}
   *
   * @throws Exception
   */
  @Test
  public void testMethods() throws Exception {
    System.out.println("test method getSize");
    TopicsScanner scanner = new TopicsScanner(folder);
    FileEntry testObj = new FileEntry(scanner.getOffsetPath("topic1", "2014-10-12-18-37-05"));
    assertEquals("Error", 0L, testObj.getSize());
    System.out.println("test method getCall");
    testObj.call();
    assertTrue("Entry initialization error!", testObj.getSize() > 0L);
    System.out.println("test method getContent");
    assertNotNull("Content initialization error", testObj.getContent());
    System.out.println("test method getStat");
    assertNotNull("Stat initialisation error", testObj.getStat());



  }

}
